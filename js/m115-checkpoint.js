/**
 * CheckPoint Object which holds all the data related to a checkpoint. It loads 
 * measurements from the google spreadsheet.
 * 
 * All checkpoints contain the following derived data:
 *   - measurementsByCompetitor:  
 *   - relativeTimesByCompetitor:  
 *   - sectionTimesByCompetitor:  
 * 
 * @param name
 * @param totalDistances the distance of this checkpoint from the start per categories. 
 * Because a checkpoint can participate in more categories we need to
 * specify it's distance from the start in every category. 
 * This param is an object with the categories as keys, distances as values. 
 */
function CheckPoint(name, totalDistances, key, gid, previousCheckPoint){
    this.name = name;
    this.totalDistances = totalDistances;
    this.key = key;
    this.gid = gid ;
    this.previousCheckPoint = previousCheckPoint == null ? this : previousCheckPoint;
    this.distanceFromPrevious = totalDistances - this.previousCheckPoint.totalDistances;
}

CheckPoint.prototype.getEditorUrl = function(){
    return 'https://spreadsheets.google.com/ccc?key=' + this.key + '&hl=hu&#gid=' + this.gid;
}

/** Loads the data from the spreadsheet of the checkpoint asynchronously */
CheckPoint.prototype.loadDataFromSpreadsheet = function(year){
    var callback = ('spreadSheet' + this.key + 'Loaded').replace(/-/g, '_');
    var obj = this;
    CheckPoint[callback] = function(data){
        obj.parseRawSpreadsheetData(data);
        obj.clearData(year);
        CheckPoint.loadNotifier();
        CheckPoint[callback] = null;
    }
    var url = 'http://spreadsheets.google.com/feeds/cells/' + this.key + '/'+ yearMapping[year]['worksheetIndex'] + 
        '/public/values?alt=json-in-script&callback=CheckPoint.' + callback;
    $.getScript(url);
}

function loadCheckPointData(year, nextMethod){
    var cpCount = checkPoints.length ;
    var loadedCount = 0;

    CheckPoint.loadNotifier = function(){
        $('#progress').progressBar(100 * (++loadedCount) / cpCount);
        if (loadedCount == cpCount) {
            $("#loading").fadeOut(300);
            $.each(checkPoints, function() {
                this.postprocessData(year) ;
            });
            nextMethod();
        }
    }
    $.each(checkPoints, function(){
        this.loadDataFromSpreadsheet(year);
    });
}

/** Parses the jsonp atom feed coming from the spreadsheet data api.
 *  Removes the first row and shifts the table 1 row up so the first data row has the index 0
 */
CheckPoint.prototype.parseRawSpreadsheetData = function(rawdata){
    tmpdata = new Array();    
    var length = 0;
    if (rawdata.feed && rawdata.feed.entry) {
        length = rawdata.feed.entry.length;       
    }
    for (var i = 0; i < length; i++) {
        var entry = rawdata.feed.entry[i];
        //shift up with 2 rows       
        var row = parseInt(entry.gs$cell.row) - 2;
        if (row == -1) {
            //Do not store the headers
            continue;
        }
        var col = parseInt(entry.gs$cell.col);
        var value = entry.content.$t;
        if (tmpdata[row] == null ) {
            tmpdata[row] = new Array();
        }
        if (col < 4 ) { 
            try {
                tmpdata[row][col] = value;
            } catch (err) {
            }
        }
    
    }
    // remove table rows without an id or with nonexistent id: 
    this.data = new Array();
    var j = 0 ;
    for (var i = 0; i < tmpdata.length; i++) {
        if ( (tmpdata[i] != null) && (tmpdata[i][1] != null) && ( tmpdata[i][1] != '' ) &&
             (competitorsById[tmpdata[i][1]] != null) ) {
             //($.inArray(tmpdata[i][1], competitorsById)) ) {
            this.data[j++] = tmpdata[i] ;
        }
    }
}

/** 
* Creates alternative internal represantations of the data to allow faster 
* access and calculates some information from the raw data
*/
CheckPoint.prototype.clearData = function(year) {
    this.measurementsByCompetitor = new Array();
    this.relativeTimesByCompetitor = new Array();
    this.sectionTimesByCompetitor = new Array();
    var competitorId = 0;
    for (i = 0; i < this.data.length; ++i) {
        competitorId = 0;
        // storing measurements in timestamps into column 4
        try {
            var measurementTime ;
            // date format constructed: 4 Jun 2011 21:50:00
            // two days are valid now:  a number greater than 1 --> second day; 
            //                          everything else --> first day
            var month;// =  yearMapping[year]['month'] ;
            var day ;
            
            if ( ( this.data[i][3] == '31' || typeof(this.data[i][3]) === 'undefined' ) ) {
                month = 'May' ;
                day = '31' ;
            }
            else {
                month = 'Jun' ;
                day = '1' ;
            }
            
            //var day = this.data[i][3] | 31;
            
            measurementTime = Date.parse(
                    day + ' ' + month +  ' ' + year + ' ' + this.data[i][2]) ;
            //
            //if ( this.data[i][3] >= 2 ) {                
            //    measurementTime = Date.parse(
            //        yearMapping[year]['dates'][1] + ' ' + month +  ' ' + year + ' ' + this.data[i][2]) ;
            //}
            //else {
            //    measurementTime = Date.parse(
            //        yearMapping[year]['dates'][0] + ' ' + month + ' ' + year + ' ' + this.data[i][2]) ;
            //}

            var measurement = new Date();
            measurement.setTime(measurementTime);
            this.data[i][4] = measurement;

            competitorId = parseInt(this.data[i][1]);
            var cat = competitorsById[competitorId].category;
            this.measurementsByCompetitor[competitorId] = measurement;
            // Storing measurement in the competitor object as the last known measurement
            if ( competitorsById[competitorId].lastCheckPoint.totalDistances[cat] <= this.totalDistances[cat] ) {
                competitorsById[competitorId].lastMeasurementTime = measurementTime;
                competitorsById[competitorId].lastCheckPoint = this;
            }
        } catch (err) {
            // unparseable lines are condidered normal            
        }
    }
}

/** 
* complete derived times after all checkpoints are read
*/
CheckPoint.prototype.postprocessData = function(year) {
    for (i = 0; i < this.data.length; ++i) {
        try {
            var competitorId = parseInt(this.data[i][1]);
            var measurementTime = this.data[i][4].getTime() ;
            this.relativeTimesByCompetitor[competitorId] = new Date(measurementTime - 
                    checkPoints[0].measurementsByCompetitor[competitorId].getTime() - 60*60*1000 );
            this.sectionTimesByCompetitor[competitorId] = new Date(measurementTime - 
                    this.previousCheckPoint.measurementsByCompetitor[competitorId].getTime() - 60*60*1000 );
        } catch (err) {
            // unparseable, erroneous lines are considered normal            
        }
    }
}


checkPoints = new Array();
var lastCP = null;

checkPoints[0] = lastCP = new CheckPoint("Rajt, Kisnána", {
    '55': 0,
    '88': 0,
    '115': 0
}, 'rkBx7rPdQBX_sdG42xKGcTw', 12, lastCP);
checkPoints.push(lastCP = new CheckPoint("Jagus", {
    '55': 6.97,
    '88': 6.97,
    '115': 6.97
}, 'rHUXntdKskPq-g7cjjGuogg', 10, lastCP));
checkPoints.push(lastCP = new CheckPoint("Oroszlánvár", {
    '55': 8.03,
    '88': 8.03,
    '115': 8.03
}, 'rU0c7yO9lnE4JMjXC9vDb5A', 8, lastCP));
checkPoints.push(lastCP = new CheckPoint("Kékestető",{
    '55': 17.15,
    '88': 17.15,
    '115': 17.15
} , 'rp3Oz3r_RbRXWMHISByz3CQ', 8, lastCP));
checkPoints.push(lastCP = new CheckPoint("Parádsasvár", {
    '55': 26.46,
    '88': 26.46,
    '115': 26.46
}, 'rpLiDyxpzck2Q_KXS1qpNjw', 8, lastCP));
checkPoints.push(lastCP = new CheckPoint("Galyatető", {
    '55': 29.2,
    '88': 29.2,
    '115': 29.2
}, 'rLvP4K9n084oPRUHcrwH01Q', 9, lastCP));
checkPoints.push(lastCP = new CheckPoint("Mátraalmás",{
    '55': -1,
    '88': 34.83,
    '115': 34.83
}, 'rUMp4t_2pXVK2sIyrk2sJQQ', 8, lastCP));
checkPoints.push(lastCP = new CheckPoint("Galyatető",{
    '55': -1,
    '88': 38.74,
    '115': 38.74
}, 'ryajsrcMAa70v2-dTddDytA', 8, lastCP));
checkPoints.push(lastCP = new CheckPoint("Hatökör-ura", {
    '55': -1,
    '88': 45.76,
    '115': 45.76
}, 'rNl2Max4wF5_N-lFFYxrx2Q', 7, lastCP));
checkPoints.push(lastCP = new CheckPoint("Mátraháza",{
    '55': -1,
    '88': 47.47,
    '115': 47.47
}, 'ri4dTnHISYkh4uNqZYHmc1w', 7, lastCP));
checkPoints.push(lastCP = new CheckPoint("Lajosháza", {
    '55': -1,
    '88': 51.93,
    '115': 51.93
}, 'rKes8IbKtYMR10JLKOLKizw', 7, lastCP));
checkPoints.push(lastCP = new CheckPoint("Mátraszentimre",{
    '55': 35.88,
    '88': 60.54,
    '115': 60.54
}, 'rdCY4iapyN4kmvcaDH0K0oQ', 7, lastCP));
checkPoints.push(lastCP = new CheckPoint("Szorospatak",{
    '55': -1,
    '88': 67.67,
    '115': 67.67
}, 'roUMt96ikA35-LCId9m3Tig', 7, lastCP));
checkPoints.push(lastCP = new CheckPoint("Ágasvári-th.",{
    '55': -1,
    '88': 71.9,
    '115': 71.9
}, 'r172GMFtdIeMLDvNSggDJAA', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Ágasvár",{
    '55': -1,
    '88': 72.46,
    '115': 72.46
}, 'rRIV9hAScChqx3flosccHTA', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Fallóskút", {
    '55': -1,
    '88': 76.11,
    '115': 76.11
}, 'rXbQCan4SfDpu6J3yLrdPMg', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Hidegkúti th.",{
    '55': 45.01,
    '88': 81.94,
    '115': 81.94
}, 'rkwgMHrdPh67yXTr3bLMXpA', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Muzsla",{
    '55': -1,
    '88': -1,
    '115': 90.01
}, 'rvp471CxGaSFKfUCPT_ADGQ', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Diós-patak",{
    '55': -1,
    '88': -1,
    '115': 95.53
}, 'r_J5HDj-SaFx1xS2AzHtf-g', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Havas",{
    '55': -1,
    '88': -1,
    '115': 104.51
}, 'rmBjdmu2TzG1D7KWX30YADA', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Fajzatpuszta",{
    '55': -1,
    '88': -1,
    '115': 107.66
}, 'rhytLj-mD9fkUtIHU2k4Bdg', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Káva", {
    '55': -1,
    '88': 80.79,
    '115': 110.06
}, 'r0kaOaZBYBnXWsh437POV-Q', 6, lastCP));
checkPoints.push(lastCP = new CheckPoint("Tót-hegyes",{
    '55': 47.71,
    '88': 84.64,
    '115': 113.91
}, 'rpDYpnAko73ye5UhlHfeugw', 7, lastCP));
checkPoints.push(lastCP = new CheckPoint("Világoshegy",{
    '55': 50.76,
    '88': 87.69,
    '115': 116.96
}, 'r_txl0wQ0P2nXWJYmAbBIuw', 7, lastCP));
checkPoints.push(lastCP = new CheckPoint("Gyöngyöstarján", {
    '55': 57.84,
    '88': 94.77,
    '115': 124.04
}, 'rS-VC6Lz6EmbcsyE7Awqyvw', 6, lastCP));


